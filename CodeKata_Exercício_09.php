<?php
/*
CodeKata.com (http://codekata.com/kata/kata09-back-to-the-checkout/)
Exercício 09
-----------------------------------------------------------------------
Foi desenvolvida em PHP pois não tenho domínido da linguagem Ruby
-----------------------------------------------------------------------
*/

class Desconto {
  private $valor, $quantidade;
  
  public function __construct($valor, $quantidade) {
    $this->valor = $valor;
    $this->quantidade = $quantidade;
  }

  public function calcular_desconto($total_item) {
    $resultado = floor($total_item / $this->quantidade) * $this->valor;
    return $resultado;
  }
}

class RegraPreco {
  private $preco_original, $descontos;

  public function __construct($preco_original, $descontos = null) {
    $this->preco_original = $preco_original;
    $this->descontos = $descontos;
  }

  public function total_prod($quantidade) {
    $resultado = ($quantidade * $this->preco_original) - $this->desconto_por($quantidade);
    return $resultado;
  }

  public function desconto_por($total_item) {
    $contador = 0;
    foreach ($this->descontos as $desconto) {
      $contador += $desconto->calcular_desconto($total_item);
    }
    return $contador;
  }
}

$RULES = array('A' => new RegraPreco(50, new Desconto(20, 3)),
               'B' => new RegraPreco(30, new Desconto(15, 2)),
               'C' => new RegraPreco(20),
               'D' => new RegraPreco(15),
              );

class CheckOut {

  private $rules, $items;

  public function __construct($rules) {
    $this->rules = $rules;
    $this->items = array();
  }

  public function scan($item) {
    if (isset($this->items[$item])) {
      $this->items[$item] += 1;
    } else {
      $this->items[$item] = 1;
    }
  }

  public function total() {
    $contador = 0;
    foreach ($this->items as $item => $quantidade) {
      $contador += $this->preco_por($item, $quantidade);
    }
    return $contador;
  }

  public function preco_por($item, $quantidade) {
    if (isset($this->rules[$item])) {
      return $this->rules[$item]->total_prod($quantidade);
    }
  }

}